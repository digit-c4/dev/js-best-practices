# NMS Best practices for devs

* JS Runtime: Node version 20
* Manage node versions using [NVM](https://github.com/nvm-sh/nvm)
* Build using [RollupJS](https://rollupjs.org/)

## Bootstrap
```shell
mkdir my-project && cd my-project
# Make sure the standard .gitignore is set
curl -o .gitignore https://raw.githubusercontent.com/github/gitignore/main/Node.gitignore
```
